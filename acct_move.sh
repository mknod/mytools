#!/bin/bash
username=$1 
server=$2

LIME_YELLOW=$(tput setaf 190)
RED=$(tput setaf 1)
NORMAL=$(tput sgr0)


#create_package packages the account and skips home directory
create_package() {
printf "${LIME}Packaging $username ${NORMAL}\n"
  /scripts/pkgacct --skiphomedir $username

}

move_package() {
printf "${LIME} moving $username to $server ${NORMAL} \n"
rsync -avzP  /home/cpmove-$username.tar.gz $server:/home/

}

restore_package() {
ssh $server "/scripts/restorepkg --force /home/cpmove-$username.tar.gz"
}

rsync_home_dir() {
    rsync --archive --verbose --drop-cache --prune-empty-dirs --compress --progress --partial /home/$username $server:/home/
}



fix_permissions() {
ssh $server chown -c -R $username:$username /home/$username/
ssh $server /scripts/mailperm $username
ssh $server chown -c $username:nobody /home/$username/public_html
}


create_package
move_package
restore_package
rsync_home_dir
fix_permissions
