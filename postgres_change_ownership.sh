#!/bin/bash 
# Postgres fix this will change ownership of all of the views and sequences 
# and tables in a specified postgres database This script is dangerous and
# could seriously break stuff please only use this if you understand what it 
# is doing by reading the script FIRST 
DATABASE_NAME=$2
NEW_USERNAME=$3
E_OPTERROR=85
# Check if user is root. We'll switch to the postgres user viathe script for ease of use.
if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 1>&2
      exit 1
fi

usage() {
   echo "Usage `basename $0` options ( -a (Change ownership of views,sequences and tables) -v (Change ownership of views) -s (change ownership of sequences) databasename newusername"
 }



if  [ $# -eq "0" ]
then usage
  
  exit E_OPTERROR
fi

get_tables_views_sequences() {
### 3 cases (tables, views, sequences) to fix the permissions on the database
# Get tables
tables=`sudo -u postgres psql -qAt -c "select tablename from pg_tables where schemaname = 'public';" $DATABASE_NAME`
#Get sequences
sequences=`sudo -u postgres psql -qAt -c "select sequence_name from information_schema.sequences where sequence_schema = 'public';" $DATABASE_NAME`
#Get views
views=`sudo -u postgres psql -qAt -c "select table_name from information_schema.views where table_schema = 'public';" $DATABASE_NAME` 
}
get_tables_views_sequences

change_table_permissions() {
### Only changing permissions on the table
for list in $tables;
do
  sudo -u postgres psql -c "alter table $list owner to $NEW_USERNAME" $DATABASE_NAME;
done
}

change_sequence_permissions() {
for list in $sequences;
do
  sudo -u postgres psql -c "alter table $list owner to $NEW_USERNAME" $DATABASE_NAME;
done
}

change_views_permissions() {
for list in $views;
do
  sudo -u postgres psql -c "alter table $list owner to $NEW_USERNAME" $DATABASE_NAME;
done
}

doall() {
change_table_permissions && change_sequence_permissions && change_views_permissions

}

while getopts ":asvdu" Option
do
 case $Option in 
   a  ) doall;;
   s  ) change_sequence_permissions;;
   v  ) change_views_permissions;;
   t  ) change_table_permissions;;
   *  ) usage;;
    esac
 done

 shift $(($OPTIND - 1)) 
