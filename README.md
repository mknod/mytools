MyTools

MyTools is simply a collection of scripts I've created to help not duplicate
work. 

Some are better written than others, and all need some work. 

Some have options and some don't. (This is a bug, and I'm going to work on it)
Nearly all of these have some danger attached to them. 
acct_move.sh

Usage: 

./acct_move.sh username servername

This will package a large cpanel account (without the home directory) then move the files over via ssh. 

====================

mail_check.sh

Usage:

mail_check.sh 

This will help compare MX records to A records for a domain to see if they
should be listed in /etc/remotedomains or /etc/localdomains

====================

diskspace_offender.py

Uses subprocess to execute ssh over multiple servers in range and execute
diskhog.sh

Need to edit the servers you want to check in the array
=====================

execute_alot.py

Uses subprocess to execute a command over multiple servers
Needs you to input the command manually by editing the file

=====================
ffmpeg-compile.sh

A bit out of date, but will install the necesssary ffmpeg libraries and ffmpeg
itself
Needs source packages updated

======================

fix_perms.sh 

Usage: fix_perms.sh username

Sets permissions to their default values based on username

Files: 644 (Read Write, Read, Read)
Directories 755 (Read write execute, read execute, read execute)
Uses's cpanel's /scripts/mailperm to fix mailbox permissions 

======================

memcached-compile.sh 

Compiles and installs memcached for the user

Need to edit /etc/init.d/memcached for the username afterward

======================

postgres_change_ownership.sh

Usage: postgres_change_ownership.sh username

Changes sequence, views, table permissions to the same role as the username
specified

======================

strainer.sh 

Usage: strainer.sh username servername

Will login to a server as root and add the strainer php.ini for a user

=====================

