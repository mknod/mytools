#!/bin/bash
# memcached is a popular caching software for php and in particular, presta shop. 
# This script will automagically isntall it for the user
export TMPDIR=/root/tmp/
DIRECTORY="/root/memcached-source"
WGET_PARAMS="-N --progress=bar:force"

if [ ! -d "$DIRECTORY"  ]; then
	mkdir $DIRECTORY
	cd $DIRECTORY
else
echo "Directory exists, will try to continue" && cd $DIRECTORY;
fi

#Check if root, if not then we can't make install, this is in a function in case we want to seperate make and make install in the future
check_root() {
if [[ $EUID -ne 0 ]]; then
#if not root, script will exit
echo 'YOU ARE NOT ROOT! You will not be able to make install or install necessary software' && exit 0;
fi
}

get_files() {
#libevent is required for memcached to compile
cd $DIRECTORY && wget $WGET_PARAMS https://github.com/downloads/libevent/libevent/libevent-2.0.21-stable.tar.gz && tar xvfz libevent-2.0.21-stable.tar.gz

#memcached
cd $DIRECTORY && wget $WGET_PARAMS http://memcached.googlecode.com/files/memcached-1.4.15.tar.gz && tar xfvz memcached-1.4.15.tar.gz
}

compile_libevent() {
cd $DIRECTORY/libevent-2.0.21-stable/ && ./configure; make; make install
}

compile_memcached() {
cd $DIRECTORY/memcached-1.4.15/ && ./configure --with-lib-event=/usr/local/; make; make install 
ln -s /usr/local/lib/libevent-2.0.so.5 /usr/lib64/libevent-2.0.so.5
ldconfig
}

start_on_boot() {
cp ./memcached /etc/init.d/memcached 
chmod 755 /etc/init.d/memcached
chkconfig --add memcached
chkconfig --level 345 memcached on
echo "Make sure to Change the USERNAME variable!"
}


check_root
get_files
compile_libevent
compile_memcached
start_on_boot
