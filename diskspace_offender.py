#!/usr/bin/python

import subprocess
offenders = ['a2s44', 'a2s49', 'a2s56', 'a2s64', 'a2s76', 'a2ss1']

'''
TODO: Collect offending servers automatically

'''


for server_name in offenders:
	print 'Here are the top 3 offenders on server ', server_name
	subprocess.call(['ssh', 'root@'+ server_name, '/app/bin/diskhog.sh'])
