#!/bin/bash
username=$1
servername=$2

	echo "./strainer.sh username servername"
	exit 1
}

[[ $# -eq 0 ]] && usage

ssh root@$servername  "echo 'SetEnv PHPRC /usr/local/lib/php-unsafe.ini' |tee -a /home/$username/.htaccess"
