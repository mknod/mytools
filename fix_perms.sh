#!/bin/bash

username=$1

fix_home() {
chown -R $username:$username /home/$username
chown  $username:nobody /home/$username/public_html
find /home/$username/public_html -type f -exec chmod 644 {} \;
find /home/$username/public_html -type d -exec chmod 755 {} \;
}

fix_mail() {
/scripts/mailperm $username
}

fix_home
fix_mail
