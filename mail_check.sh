#!/bin/bash
# Script to check for the mail dns settings from a domain 
# Usage: mail_check.sh domain.com
DOMAIN=$1

mx_record() {
    MX=$(dig +short MX $DOMAIN)
echo "The MX record for $DOMAIN is:"
echo $MX
echo "The MX record for $DOMAIN points to the address:"
dig +short $MX

}

a_record() {
echo "The A record for $DOMAIN is:"
dig +short $DOMAIN
}



mx_record
a_record
