#!/bin/bash 
# Gerald Stuhrberg
# Script is based on the guide here: https://ffmpeg.org/trac/ffmpeg/wiki/CentosCompilationGuide 
# This script will install yasm, lame, libogg, libvorbis, zlib, x264, libvpx, ffmpeg from source. These# require the following from yum: gcc, git, make, nasm, pkgconfig, wget
export TMPDIR=/root/tmp/
DIRECTORY="/root/ffmpeg-source"
CURL_OPTS="-O"

#Check if root, if not then we can't make install, this is in a function in case we want to seperate make and make install in the future
check_root () {
if [[ $EUID -ne 0 ]]; then
#if not root, script will exit
echo 'YOU ARE NOT ROOT! You will not be able to make install or install necessary software' && exit 0;
fi
}

check_redhat() {
yum install redhat-lsb 
if grep -q -i "release 6" /etc/redhat-release
then
      echo "running RHEL 6.x Please use yum to install the packages" && exit 0
  fi

}

#install software via yum
check_root 

yum install gcc git make nasm pkgconfig wget

#Check if directory exists, if not create it
if [ ! -d "$DIRECTORY" ]; then
	mkdir $DIRECTORY
	cd $DIRECTORY
else
echo "Directory ffmpeg-source exists will try to continue" && cd $DIRECTORY;

fi

yasm_install() {
cd $DIRECTORY
curl -O http://www.tortall.net/projects/yasm/releases/yasm-1.2.0.tar.gz
tar xzvf yasm-1.2.0.tar.gz
cd yasm-1.2.0
./configure
make
make install
make distclean
}

x264_install() {
cd $DIRECTORY
git clone --depth 1 git://git.videolan.org/x264
cd x264
make
make install
make distclean
}

libfdk_install() {
cd $DIRECTORY
git clone --depth 1 git://git.code.sf.net/p/opencore-amr/fdk-aac
cd fdk-aac
autoreconf -fiv
./configure --prefix="$HOME/ffmpeg_build" --disable-shared
make
make install
make distclean
}

libmp3lame_install() {
cd $DIRECTORY
curl -L -O http://downloads.sourceforge.net/project/lame/lame/3.99/lame-3.99.5.tar.gz
tar xzvf lame-3.99.5.tar.gz
cd lame-3.99.5
./configure --enable-nasm
make
make install
make distclean
}

libopus_install() {
cd $DIRECTORY
curl -O http://downloads.xiph.org/releases/opus/opus-1.0.3.tar.gz
tar xzvf opus-1.0.3.tar.gz
cd opus-1.0.3
make
make install 
make distclean
}

libogg_install() {
cd $DIRECTORY
curl -O http://downloads.xiph.org/releases/ogg/libogg-1.3.1.tar.gz
cd libogg-1.3.1
./configure
make
make install
make distclean
}

libvorbis_install() {
cd $DIRECTORY
curl -O http://downloads.xiph.org/releases/vorbis/libvorbis-1.3.3.tar.gz
tar xvfz libvorbis-1.3.3.tar.gz
cd libvorbis-1.3.3
./configure
make 
make install
make distclean
}

libvpx_install() {
cd $DIRECTORY
git clone --depth 1 http://git.chromium.org/webm/libvpx.git
cd libvpx
./configure --disable-examples
make
make install
make clean
}

ffmpeg_install() {
cd $DIRECTORY
git clone --depth 1 git://source.ffmpeg.org/ffmpeg
cd ffmpeg
./configure --enable-gpl --enable-nonfree --enable-libfdk_aac --enable-libmp3lame --enable-libopus --enable-libvorbis --enable-libvpx --enable-libx264
make
make install
make distclean
}

yasm_install
x264_install
libfdk_install
libmp3lame_install
libopus_install
libogg_install
libvorbis_install
libvpx_install
ffmpeg_install
